import requests
from lxml import html


class WikiContent:

    def __init__(self):
        self.__word_srch = None
        self.__history_dict = dict()
        self.__url_pattern = 'https://cs.wikipedia.org/w/index.php?search={}' \
                             '&title=Speci%C3%A1ln%C3%AD%3AHled%C3%A1n%C3%AD&go=J%C3%ADt+na&wprov=acrw1_-1'

    @property
    def word_srch(self):
        return self.__word_srch

    @word_srch.setter
    def word_srch(self, word_srch):
        self.__word_srch = word_srch.strip()

    def check_history(self):
        """
        kontrolo jestli uz se nekdy v minulosti nevyhledalo
        :return:
        """
        if self.word_srch in self.__history_dict:
            return self.__history_dict.get(self.word_srch)

    def get_url(self, url):
        """
        rekurzivne projde vsechny presmerovavani az na koncovou stranku
        :param url: url
        :return: valid url
        """
        res = requests.head(url)
        if res.next:
            result = self.get_url(res.next.url)
        else:
            result = res.url
        return result

    def logic(self):
        """
        zakladni logika - ziskava text z konecnehe url
        :return: status (jestli se neco naslo nebo ne), text (prvni odstavec nebo nazvy clanku)
        """

        if result := self.check_history():
            return result['status'], result['result']

        else:
            url_request = self.__url_pattern.format('+'.join(self.word_srch.split(' ')))
            url_correct = self.get_url(url_request)

            response = requests.get(url_correct)
            pg_html = html.fromstring(response.text)
            pg_head = pg_html.xpath('//*[@id = "firstHeading"]//text()')

            if 'Výsledky hledání' not in pg_head:
                parags = pg_html.xpath('//*[@id = "mw-content-text"]//p')
                text = ' '.join(parags[0].xpath('.//text()'))
                self.__history_dict[self.word_srch] = dict(status=True, result=text)
                return True, text
            else:
                articles = pg_html.xpath('//ul[@class = "mw-search-results"]/li')
                articles_head = []
                if articles:
                    articles_head = [''.join(art.xpath('./div[@class = "mw-search-result-heading"]//text()')).strip()
                                     for art in articles]
                text = '\n'.join(articles_head)
                self.__history_dict[self.word_srch] = dict(status=False, result=text)
                return False, text

    def find_word(self):
        """
        Vypise vysledek pokud je zadane slovo
        :return: None
        """
        if self.word_srch:
            status, text = self.logic()

            if status:
                print(f'{self.word_srch}: {text}')
            else:
                print(f'Článek s názvem  \'{self.word_srch}\' nebyl nalezen,'
                      f' zadaný text se vyskytuje v článcích s tímto názvem:\n'
                      f'{text}')
        else:
            print('Prosím zadej nějaké slovo nikoliv jen mezery nebo nic')


if __name__ == '__main__':
    hledani = WikiContent()
    sl = ''
    while sl != 'konec':
        sl = input('zadej nejake slovo nebo pro konec napis \'konec\': ')
        if sl != 'konec':
            hledani.word_srch = sl
            hledani.find_word()
